package net.ciprianlungu.GUI;

import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.JComboBox;
import javax.swing.JTextField;

import net.ciprianlungu.modelo.Eficiencia;
import net.ciprianlungu.modelo.GestorCoches;
import net.ciprianlungu.modelo.Marca;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CrearModelo extends JPanel {
	private JTextField tfModelo;
	private JTextField tfConsumo;
	private JTextField tfEmisiones;
	GestorCoches gc = new GestorCoches();
	/**
	 * Create the panel.
	 */
	public CrearModelo() {
		setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 800, 600);
		add(panel);
		panel.setLayout(null);
		
		//LABEL INSERTAR MODELO
		JLabel lblCrearCoches = new JLabel("Insertar modelo\r\n");
		lblCrearCoches.setBounds(300, 11, 188, 28);
		panel.add(lblCrearCoches);
		lblCrearCoches.setFont(new Font("Tahoma", Font.BOLD, 23));
		
		//LABEL MARCA
		JLabel lblNewLabel = new JLabel("Marca:");
		lblNewLabel.setBounds(50, 70, 60, 20);
		panel.add(lblNewLabel);
		lblNewLabel.setFont(new Font("SimSun", Font.BOLD, 17));
		
		//LABEL MODELO
		JLabel lblNewLabel_1 = new JLabel("Modelo:");
		lblNewLabel_1.setBounds(40, 110, 70, 20);
		panel.add(lblNewLabel_1);
		lblNewLabel_1.setFont(new Font("SimSun", Font.BOLD, 17));
		
		//LABEL CONSUMO
		JLabel lblNewLabel_2 = new JLabel("Consumo:");
		lblNewLabel_2.setBounds(30, 150, 80, 20);
		panel.add(lblNewLabel_2);
		lblNewLabel_2.setFont(new Font("SimSun", Font.BOLD, 17));
		
		//LABEL EMISIONES
		JLabel lblNewLabel_3 = new JLabel("Emisiones:");
		lblNewLabel_3.setBounds(10, 190, 100, 20);
		panel.add(lblNewLabel_3);
		lblNewLabel_3.setFont(new Font("SimSun", Font.BOLD, 17));
		
		//LABEL CALIFICACION ENERGETICA
		JLabel lblNewLabel_4 = new JLabel("Calificacion Energetica:");
		lblNewLabel_4.setBounds(10, 230, 240, 20);
		panel.add(lblNewLabel_4);
		lblNewLabel_4.setFont(new Font("SimSun", Font.BOLD, 17));
		
		
		//TEXTFIELD DE MODELO
		tfModelo = new JTextField();
		tfModelo.setBounds(120, 110, 232, 20);
		panel.add(tfModelo);
		tfModelo.setColumns(10);
		
		//TEXTFIELD DE CONSUMO
		tfConsumo = new JTextField();
		tfConsumo.setBounds(120, 150, 60, 20);
		panel.add(tfConsumo);
		tfConsumo.setColumns(10);
		
		//TEXTFIELD DE EMISIONES
		tfEmisiones = new JTextField();
		tfEmisiones.setBounds(120, 190, 86, 20);
		panel.add(tfEmisiones);
		tfEmisiones.setColumns(10);
		

		//BOTON GUARDAR
		JButton JbGuardar = new JButton("");
		JbGuardar.setIcon(new ImageIcon(CrearModelo.class.getResource("/assets/Save.jpg")));
		JbGuardar.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0){
				
			}
		});
		JbGuardar.setBounds(127, 291, 53, 54);
		panel.add(JbGuardar);
		
		
		//BOTON LIMPIAR
		JButton JbLimpiar = new JButton("");
		JbLimpiar.setIcon(new ImageIcon(CrearModelo.class.getResource("/assets/Power - Restart.jpg")));
		JbLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("LIMPIO");
			}
		});
		JbLimpiar.setBounds(197, 291, 53, 54);
		panel.add(JbLimpiar);
		
		
		//COMBO DE MARCAS
		ArrayList<Marca> marcas = gc.getMarcas();
		JComboBox comboBoxMarca = new JComboBox();
		comboBoxMarca.setBounds(120, 70, 142, 22);
		for(Marca marcaa : marcas){
			comboBoxMarca.addItem(marcaa.getMarca());
		}
		panel.add(comboBoxMarca);
		
		
		//COMBO DE CALIFICACION ENERGETICA
		ArrayList<Eficiencia> eficiencias = gc.getEficiencias();
		JComboBox comboBoxCEnergetica = new JComboBox();
		comboBoxCEnergetica.setBounds(263, 230, 53, 22);
		for(Eficiencia eficienciaa : eficiencias){
			comboBoxCEnergetica.addItem(eficienciaa.getCalificacion());
		}
		panel.add(comboBoxCEnergetica);

	}
}
