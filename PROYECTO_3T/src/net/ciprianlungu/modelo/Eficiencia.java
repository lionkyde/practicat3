package net.ciprianlungu.modelo;

public class Eficiencia {
	private String calificacion;

	public Eficiencia(String calificacion) {
		super();
		this.calificacion = calificacion;
	}

	public String getCalificacion() {
		return calificacion;
	}

	public void setCalificacion(String calificacion) {
		this.calificacion = calificacion;
	}
}
